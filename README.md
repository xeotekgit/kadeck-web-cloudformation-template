# Kadeck Web CloudFormation Template

The cloud formation template creates a Security Group and an EC2 instance with KaDeck Web.

# Create a free team first

Make sure that you have already created a free team via https://www.getkadeck.com. A team automatically includes a free user that you can use to configure and use KaDeck Web. 